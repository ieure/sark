FROM debian:bookworm-slim as build
RUN apt update
RUN apt -y upgrade
RUN apt -y --no-install-recommends install openjdk-17-jdk-headless leiningen
RUN apt -y --no-install-recommends install curl

RUN mkdir /build
COPY . /build

# Update the index
WORKDIR /build/resources
RUN curl -so index.txt https://arcarc.xmission.com/index.txt
RUN curl -sI https://arcarc.xmission.com/index.txt | grep ^Last-Modified | cut -d\  -f2-999 > index.txt.lmd

# Build and test
WORKDIR /build
RUN lein deps
RUN lein do test, uberjar

# Create output image
FROM debian:bookworm-slim as runtime

RUN apt update
RUN apt -y upgrade
RUN apt -y --no-install-recommends install openjdk-17-jre-headless
RUN mkdir /opt/sark
COPY --from=build /build/target/sark-*-standalone.jar /opt/sark/sark.jar
CMD ["java", "-jar", "/opt/sark/sark.jar"]
EXPOSE 3000/tcp
