#+BEGIN_SRC
eeeee eeeee eeeee  e   e
8   " 8   8 8   8  8   8
8eeee 8eee8 8eee8e 8eee8e
   88 88  8 88   8 88   8
8ee88 88  8 88   8 88   8
#+END_SRC

Sark is a tool for searching the contents of [[http://arcarc.xmission.com][ArcArc]], the Arcade
Archive.  ArcArc has thousands of documents for coin-operated
machines, and Sark makes it easy to find them.

** Hacking on Sark
   :PROPERTIES:
   :ID:       c16fa306-6932-4da6-a6a0-03abd3cba0dc
   :END:

   You’ll need Leiningen 2.0 or better.

   For development, it should be as simple as running =lein ring
   server=.  For running Sark in production, you can either build an
   uberjar (=lein uberjar=) and run it with:

   #+BEGIN_SRC shell
     java -jar target/sark-*-standalone.jar
   #+END_SRC

   …or you can build a war (=lein ring war=) and use your favorite
   servlet container.
