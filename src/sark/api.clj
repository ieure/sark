;; -*- coding: utf-8 -*-
;;
;; eeeee eeeee eeeee  e   e
;; 8   " 8   8 8   8  8   8
;; 8eeee 8eee8 8eee8e 8eee8e
;;    88 88  8 88   8 88   8
;; 8ee88 88  8 88   8 88   8
;;
;; © 2013, 2014, 2022 Ian Eure.
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns sark.api
  (:require [clojure.java.io :as io]
            [compojure.core :refer [defroutes GET PUT]]
            [compojure.handler :as handler]
            [ring.middleware.file-info :refer [wrap-file-info]]
            [ring.middleware.format-response :refer [wrap-json-response]]
            [ring.middleware.resource :refer [wrap-resource]]
            [ring.util.response :as r]
            [sark.core :as sark]
            [sark.meters :as meters]))

(def ^:constant bounce
  (-> (r/redirect "/index.html")
      (r/status 301)))

(def ^:constant nil-resp (r/response []))

(defroutes sark-routes
  ;; Front page
  (GET "/" [] (io/resource "web/index.html"))

  ;; Return search results
  (GET "/s" {{q "q"} :query-params}
       (if-not (empty? q) (sark/search @sark/index q)
               nil-resp))

  ;; Record clickthrough
  (PUT "/c" {{d "d"} :query-params}
       (do (meters/click! d)
           nil-resp))

  ;; Stats
  (GET "/stats" [] (r/response (sark/stats))))

(def handler
  (-> sark-routes
      (handler/api)
      (wrap-json-response)
      (wrap-resource "web")
      (wrap-file-info)))
