;; -*- coding: utf-8 -*-
;;
;; eeeee eeeee eeeee  e   e
;; 8   " 8   8 8   8  8   8
;; 8eeee 8eee8 8eee8e 8eee8e
;;    88 88  8 88   8 88   8
;; 8ee88 88  8 88   8 88   8
;;
;; © 2013, 2014, 2020, 2022, 2023 Ian Eure.
;; Author: Ian Eure <ieure@simple.com>
;;
(ns sark.core
  (:require [clojure.string :as str]
            [clucy.core :as clucy]
            [sark.analyzer :as anal]
            [sark.arcarc :as arcarc]
            [sark.meters :as meters]
            [taoensso.timbre :as log])
  (:import [org.apache.lucene.index DirectoryReader]
           [org.apache.lucene.store Directory]))

(def ^:const update-interval (* 60 60 24 1000)) ; 24h

(def index (atom nil))

(def ^:constant shitpost-re
  "A regexp of files which won't be indexed."
  #"(^(arcade by title|pictures|utils|icons fonts etc|kits and hacks|magazines and books|pinball/pictures|videos and sounds|web archives).*|.*thumbs.db$|.*index.txt.*|)")

(defn shitpost? "Is this file a shitpost?" [name]
  (re-matches shitpost-re (str/lower-case name)))

(defn clean-strip-punc "Strip underscores." [text]
  (str/replace text #"_" " "))

(defn clean-strip-extension "Strip file extensions." [text]
  (let [li (.lastIndexOf text ".")]
    (subs text 0 (if  (> li 0) li (count text)))))

(defn clean-fix-typos "Fix typos in ArcArc." [text]
  (str/replace text #"[Cc]ab[ea]rat" "Cabaret"))

(defn clean-strip-prefix "Strip the prefix for some types." [text]
  (cond
   (.startsWith text "PDF") (let [li (.lastIndexOf text "/")]
                              (subs text (if (> li 0) (+ li 1) 0)))
   (.startsWith text "Tech") (subs text 5)
   :else text))

(defn clean-name "Clean filename." [name]
  (-> (clean-strip-punc name)
      (clean-strip-extension)
      (clean-fix-typos)
      (clean-strip-prefix)
      (str/trim)))

(defn boost "How much should we boost this document?" [name]
  (cond (.startsWith name "PDF") 1.0
        :else 0.0))

(defn make-doc "Turn a filename into a document to index." [name]
  (with-meta {:name (clean-name name), :url (str arcarc/base name)}
              {:url {:indexed false
                     :stored true}
               :name {:boost (boost name)}}))

(defn build-index "Create an index of filenames."
  ([names] (build-index (clucy/memory-index) names))

  ([idx names]
     (anal/with-analyzer
       (apply clucy/add idx (map make-doc (remove shitpost? names))))
     idx))

(defn update! []
  (when-let [{:keys [:files] :as updated-state} (arcarc/fetch-update)]
    (reset! arcarc/state updated-state)
    (reset! index (build-index files))
    (log/infof "Reindexed %d documents" (count files))))

(defn update-periodically! []
  (log/infof "Refreshing index every %dms" update-interval)
  (doto (Thread. (fn []
                   (update!)
                   (Thread/sleep update-interval)
                   (recur)))
    (.setDaemon true)
    (.start)))

(defn init-cache! []
  (reset! index (build-index (:files (arcarc/load-cache!)))))

(defn init "Initialize Sark." []
  (init-cache!)
  (update-periodically!))

(defn search [index terms & [limit]]
  (anal/with-analyzer
    (let [res (clucy/search index terms (or limit 100)
                            :default-operator :and)]
      (meters/searched! res))))

(defn explain [index terms & [limit]]
  (anal/with-analyzer
    (clucy/search index terms (or limit 100)
                  :default-operator :and :explain true)))

(defn stats "Return statistics about Sark." []
  (with-open [r (DirectoryReader/open ^Directory @index)]
    {:disk {:index-last-updated (:lmd @arcarc/state)
            :documents (.numDocs r)}
     :mem (arcarc/status)
     :searches (meters/stats)
     :clicks @meters/clicked}))
