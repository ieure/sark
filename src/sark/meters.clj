;; -*- coding: utf-8 -*-
;;
;; eeeee eeeee eeeee  e   e
;; 8   " 8   8 8   8  8   8
;; 8eeee 8eee8 8eee8e 8eee8e
;;    88 88  8 88   8 88   8
;; 8ee88 88  8 88   8 88   8
;;
;; © 2014, 2022 Ian Eure.
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns sark.meters
  (:require
   [metrics.core :refer [new-registry]]
   [metrics.histograms :refer [defhistogram update! percentiles mean]]
   [metrics.meters :refer [defmeter mark! rates] :as meter]
   [taoensso.timbre :as log]))

(def reg (new-registry))

(defmeter reg searches)

(defhistogram reg results)

(defmeter reg clicks)

(defn searched! [sres]
  (mark! searches)
  (update! results (count sres))
  sres)

 ;; Click tracking

(def ^:const sorted
  "An empty sorted map, to save results in."
  (sorted-map-by (fn [key1 key2]
                   (compare [(get results key2) key2]
                            [(get results key1) key1]))))

(def clicked "A sorted map of top-clicked results."
  (agent sorted))

(def ^:const click-size "Number of top-clicked results to display."
  10)

(defn- click* "Record a click" [state doc]
  (into sorted (update-in state [doc] (fn [n] (if n (inc n) 1)))))

#_(defn- prune "Prune the click map.

   The map will be no more than 5x of click-size."
  [state]
  (let [tn (* click-size 5)]
    (if (> (count state) tn)
      (into sorted (take tn state))
      state)))

(defn click! "Save a click on a document" [doc]
  (log/debugf "Clicking `%s'" doc)
  (send clicked click* doc))

#_(defn clicked-top "Return the most clicked results" [state]
  (map first (take click-size @clicked)))



(defn stats []
  {:searches {:rates (rates searches)
              :count (meter/count searches)}
   :results {:percentiles (percentiles results)
             :mean (mean results)}})
