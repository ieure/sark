;; -*- coding: utf-8 -*-
;;
;; eeeee eeeee eeeee  e   e
;; 8   " 8   8 8   8  8   8
;; 8eeee 8eee8 8eee8e 8eee8e
;;    88 88  8 88   8 88   8
;; 8ee88 88  8 88   8 88   8
;;
;; © 2013, 2014, 2021, 2022 Ian Eure.
;; Author: Ian Eure <ian.eure@gmail.com>
;;
(ns sark.core-test
  (:require [clojure.test :as t]
            [sark.core :as sark]))

(defn- initialize-and-run [f]
  (sark/init-cache!)
  (f))

(t/use-fixtures :once initialize-and-run)

(t/deftest test-searches
  (t/testing "Searching for schematics works."
    (let [res (sark/search @sark/index "tempest schematic")]
      (t/is (re-find #"Tempest DP" (or (:name (first res)) "")))))

  (t/testing "Searching for `dk manual' returns Donkey Kong hits"
    (let [res (sark/search @sark/index "dk manual")]
      (t/is (re-find #"(?i)donkey kong" (or (:name (first res)) "")))))

  (t/testing "Searching for `dkjr manual' returns Donkey Kong Junior hits"
    (let [res (sark/search @sark/index "dkjr manual")]
      (t/is (re-find #"(?i)donkey kong jr" (or (:name (first res)) ""))))))
